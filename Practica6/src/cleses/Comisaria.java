package cleses;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;


// TODO: Auto-generated Javadoc
/**
 * The Class Comisaria.
 */
public class Comisaria {

	/** The caballeria. */
	ArrayList<PoliciaCaballeria> caballeria;
	
	/** The nacional. */
	ArrayList<PoliciaNacional> nacional;
	
	/** The comisario. */
	ArrayList<Comisario> comisario;
	
	/** The in. */
	Scanner in = new Scanner(System.in);
	
	/**
	 * Instantiates a new comisaria.
	 */
	/*
	 * Constructor que no recibe ningun parametro solo incia los ArrayLists
	 * 
	 * */
	public Comisaria() {
		this.caballeria = new ArrayList<PoliciaCaballeria>();
		this.nacional = new ArrayList<PoliciaNacional>();
		this.comisario = new ArrayList<Comisario>();
	
	}
	
	/**
	 * Mostrar comisaria.
	 */
	/*
	 * Metodo que imprime por consola todos los policias de la comisaria
	 * */
	public void mostrarComisaria() {
		listarPoliciaCaballeria();
		listarPoliciaNacional();
		listarComisario();
		
	}
	
	/**
	 * Alta comisario.
	 *
	 * @param nombre the nombre
	 * @param dni the dni
	 * @param localidad the localidad
	 * @param a�os the a�os
	 * @param antiguedad the antiguedad
	 * @param salario the salario
	 */
	/*
	 * @param nombre, dni, localidad, a�os, antiguedad, salario
	 * Metodo que a�ade un nuevo comisario a el ArrayList comisario
	 * 
	 * */
	public void altaComisario(String nombre, String dni, String localidad, int a�os, 
			int antiguedad, double salario) {
		Comisario nuevoComisario = new Comisario(nombre,dni,localidad,a�os,antiguedad,salario);
		comisario.add(nuevoComisario);
		}
	
	/**
	 * Alta caballeria.
	 *
	 * @param nombre the nombre
	 * @param dni the dni
	 * @param localidad the localidad
	 * @param a�os the a�os
	 * @param antiguedad the antiguedad
	 * @param salario the salario
	 * @param a�osAnimal the a�os animal
	 * @param nombreAnimal the nombre animal
	 */
	/*
	 * @param nombre, dni, localidad, a�os, antiguedad, salario, a�osAnimal, nombreAnimal
	 * Metodo que a�ade un nuevo policia de caballeria a el ArrayList caballeria
	 * 
	 * */
	public void altaCaballeria(String nombre, String dni, String localidad, int a�os, 
			int antiguedad, double salario, int a�osAnimal, String nombreAnimal) {
		if(!existePoliciaCab(dni)) {
			PoliciaCaballeria nuevoPoliciaCaballeria= new PoliciaCaballeria(nombre,dni,localidad,a�os,antiguedad,salario,a�osAnimal,nombreAnimal);
			caballeria.add(nuevoPoliciaCaballeria);
		}
	}
	
	/**
	 *
	 * @param dni 
	 * Metodo que nos dice si exite un policia de caballeria
	 * @return true, si existe
	 */
	
	public boolean existePoliciaCab(String dni) {
		for (PoliciaCaballeria policiaCaballeria : caballeria) {
			if(policiaCaballeria != null && policiaCaballeria.getDni().equals(dni)) {
				return true;
			}
		}
		return false;
	}
	/*
	 *Metodo que nos lista todos los policias de caballeria de la comisaria 
	 */
	
	public void listarPoliciaCaballeria() {
		for (PoliciaCaballeria policiaCaballeria : caballeria) {
			
			if(policiaCaballeria!=null) {
				System.out.println(policiaCaballeria.toString());
			}
		}
		
	}
	
	
	/*
	 * @param dni
	 * Busca un policia de caballeria dentro del ArrayList caballeria por dni
	 * */
	public void buscarPoliciaCaballeria(String dni) {
		
		for (PoliciaCaballeria policiaCaballeria : caballeria) {
			if(policiaCaballeria.getDni().equals(dni)) {
				System.out.println("El policia con ese DNI es: " + policiaCaballeria.toString());
			}
		}
	}
	
	
	/*
	 * @param dni
	 * Borra a un policia de caballeria del ArrayList por dni
	 * */
	public void borrarPoliciaCaballeria(String dni) {
		Iterator<PoliciaCaballeria> iteratorCaballeria = caballeria.iterator();
		
		while(iteratorCaballeria.hasNext()) {
			PoliciaCaballeria caballeria = iteratorCaballeria.next();
			if(caballeria.getDni().equals(dni)) {
				iteratorCaballeria.remove();
			}
		}
	}
	
	/**
	 * Alta nacional.
	 *
	 * @param nombre 
	 * @param dni 
	 * @param localidad 
	 * @param a�os 
	 * @param antiguedad
	 * @param salario
	 * @param arma
	 * Metodo que a�ade un nuevo policia nacional a el ArrayList nacional
	 */
	
	public void altaNacional(String nombre, String dni, String localidad, int a�os, 
			int antiguedad, double salario, String arma) {
		if(!existePoliciaNac(dni)) {
			PoliciaNacional nuevoPoliciaNacional= new PoliciaNacional(nombre,dni,localidad,a�os,antiguedad,salario,arma);
			nacional.add(nuevoPoliciaNacional);
		}
	}
	
	/**
	 *
	 * @param dni 
	 * @return true, si existe
	 * Metodo que nos dice si exite un policia nacional
	 */
	
	public boolean existePoliciaNac(String dni) {
		for (PoliciaNacional policiaNacional : nacional) {
			if(policiaNacional != null && policiaNacional.getDni().equals(dni)) {
				return true;
			}
		}
		return false;
	}
	
	/*
	 *Metodo que nos lista todos los policias nacionales de la comisaria 
	 */
	public void listarPoliciaNacional() {
		for (PoliciaNacional policiaNacional: nacional) {
			
			if(policiaNacional!=null) {
				System.out.println(policiaNacional.toString());
			}
		}
		
	}
	
	/**
	 * Buscar policia nacional.
	 *
	 * @param dni 
	 * @return ArrayList<PoliciaNacional>
	 */
	public ArrayList<PoliciaNacional> buscarPoliciaNacional(String dni) {
		
		for (PoliciaNacional policiaNacional : nacional) {
			if(policiaNacional.getDni().equals(dni)) {
				System.out.println("El policia con ese DNI es: " + policiaNacional.toString());
				return nacional;
			}
		}
		return null;
	}
	
	/**
	 *
	 * @param dni 
	 * Borra un policia nacional por dni
	 */
	public void borrarPoliciaNacional(String dni) {
		Iterator<PoliciaNacional> iteratorNacional = nacional.iterator();
		
		while(iteratorNacional.hasNext()) {
			PoliciaNacional nacional = iteratorNacional.next();
			if(nacional.getDni().equals(dni)) {
				iteratorNacional.remove();
			}
		}
	}
	
	/**
	 *
	 * @param a�o
	 * Lista a un policia Nacional por a�o
	 */
	public void listarPoliciaNacionalA�o(int a�o) {
		for (PoliciaNacional policiaNacional: nacional) {
			
			if(policiaNacional!=null) {
				if(policiaNacional.getA�os()==a�o) {
					System.out.println(policiaNacional.toString());
				}
			}
		}
		
	}
	
	/**
	 *
	 * @param nombreCaballo
	 * Elimina a un caballo de un agente de caballeria por el nombre del caballo
	 */
	public void jubilarCaballo(String nombreCaballo) {
		
		for (PoliciaCaballeria policiaCaballeria : caballeria) {
			if(policiaCaballeria.getNombreAnimal().equals(nombreCaballo)) {
				policiaCaballeria.setNombreAnimal(null);
				policiaCaballeria.setA�osAnimal(0);
			}
		}
	}
	
	/**
	 * Cambia a un policia a un regimiento 
	 */
	public void cambiarDeRegimiento() {
		System.out.println("�A que regimineto quieres cambiar caballeria o nacional?");
		
		
		String respuesta = in.nextLine();
		
		
		if(respuesta.equalsIgnoreCase("caballeria")) {
			System.out.println("Escribe el dni del policia que quieres cambiar de regimiento");
			listarPoliciaCaballeria();
			String dni = in.nextLine();
			
			
			System.out.println("�Que arma quieres equipar?");
			String arma = in.nextLine();
			
			for (PoliciaCaballeria policiaCaballeria : caballeria) {
			
				if(policiaCaballeria.getDni().equals(dni)) {
					int antiguedadV =policiaCaballeria.getAntiguedad();
					int a�oV =policiaCaballeria.getA�os();
					String dniV = policiaCaballeria.getDni();
					String nombreV = policiaCaballeria.getNombre();
					String localidadV = policiaCaballeria.getLocalidad();
					double salarioV = policiaCaballeria.getSalario();
					altaNacional(nombreV, dniV, localidadV, a�oV, antiguedadV, salarioV, arma);
					borrarPoliciaCaballeria(dni);
				}
			}
			
			
		}else if(respuesta.equalsIgnoreCase("nacional")) {
			System.out.println("Escribe el dni del policia que quieres cambiar de regimiento");
			listarPoliciaNacional();
			String dni = in.nextLine();
			
			
			System.out.println("Cual es el nombre del caballo?");
			String nombreAnimal = in.nextLine();
			System.out.println("Cuantos a�os tiene el caballo?");
			int a�osAnimal = in.nextInt();
			in.nextLine();
			for (PoliciaNacional policiaNacional : nacional) {
				
				if(policiaNacional.getDni().equals(dni)) {
					int antiguedadV =policiaNacional.getAntiguedad();
					int a�oV =policiaNacional.getA�os();
					String dniV = policiaNacional.getDni();
					String nombreV = policiaNacional.getNombre();
					String localidadV = policiaNacional.getLocalidad();
					double salarioV = policiaNacional.getSalario();
					
					altaCaballeria(nombreV, dniV, localidadV, a�oV, antiguedadV, salarioV ,a�osAnimal, nombreAnimal);
					borrarPoliciaNacional(dni);
					
				}
			}
			
			
		}
		
	}
	
	/**
	 * A�ade personal al comisario.
	 */
	public void a�adirPersonalAlComisario() {
		ArrayList<String> policiaA�adir = new ArrayList<String>();
		for (int i = 0; i < 2; i++) {
			
			System.out.println("Selecciona 1 dni de los sisguientes");
			mostrarComisaria();
			String dniElegido = in.nextLine(); 
			
			for (PoliciaCaballeria policiaCaballeria : caballeria) {
				
				if(policiaCaballeria.getDni().equals(dniElegido)) {
					policiaA�adir.add(policiaCaballeria.getNombre());
				}
			}
			for (PoliciaNacional policiaNacional : nacional) {
				if(policiaNacional.getDni().equals(dniElegido)) {
					policiaA�adir.add(policiaNacional.getNombre());
				}
			}
		}

		for (Comisario comisario : comisario) {
			comisario.setPersonasAsociadas(policiaA�adir);
			comisario.setNumPersonasAsociadas(2);
		}
		
	}
	
	/**
	 * Lista a el comisario.
	 */
	public void listarComisario() {
		for (Comisario comisario : comisario) {
			
			if(comisario!=null) {
				System.out.println(comisario.toString());
			}
		}
	}
	
}
