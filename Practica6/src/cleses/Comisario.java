package cleses;

import java.util.ArrayList;

public class Comisario extends Policia{

	int numPersonasAsociadas;
	ArrayList<String> personasAsociadas = new ArrayList<String>(); 
	
	public Comisario(String nombre, String dni, String localidad, int a�os, int antiguedad, double salario) {
		super(nombre, dni, localidad, a�os, antiguedad, salario);
		
	}

	public int getNumPersonasAsociadas() {
		return numPersonasAsociadas;
	}

	public void setNumPersonasAsociadas(int numPersonasAsociadas) {
		this.numPersonasAsociadas = numPersonasAsociadas;
	}

	public ArrayList<String> getPersonasAsociadas() {
		return personasAsociadas;
	}

	public void setPersonasAsociadas(ArrayList<String> personasAsociadas) {
		this.personasAsociadas = personasAsociadas;
	}

	@Override
	public String toString() {
		return "Comisario " + super.toString() + "[numPersonasAsociadas=" + numPersonasAsociadas + ", personasAsociadas=" + personasAsociadas
				+ "]";
	}

	
}
