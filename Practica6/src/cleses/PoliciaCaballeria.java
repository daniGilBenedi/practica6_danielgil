package cleses;

public class PoliciaCaballeria extends Policia{

	private int a�osAnimal;
	private String nombreAnimal;
	
	public PoliciaCaballeria(String nombre, String dni, String localidad, int a�os, 
			int antiguedad, double salario, int a�osAnimal, String nombreAnimal) {
		super(nombre, dni, localidad, a�os, antiguedad, salario);
		this.a�osAnimal =a�osAnimal;
		this.nombreAnimal = nombreAnimal;
	}
	
	public PoliciaCaballeria(String nombre, String dni, String localidad, int a�os, 
			int antiguedad, double salario) {
		super(nombre, dni, localidad, a�os, antiguedad, salario);
	}

	

	public int getA�osAnimal() {
		return a�osAnimal;
	}

	public void setA�osAnimal(int a�osAnimal) {
		this.a�osAnimal = a�osAnimal;
	}

	public String getNombreAnimal() {
		return nombreAnimal;
	}

	public void setNombreAnimal(String nombreAnimal) {
		this.nombreAnimal = nombreAnimal;
	}
	@Override
	public String toString() {
		return "PoliciaCaballeria" + super.toString() + "[a�osAnimal=" + a�osAnimal + ", nombreAnimal=" + nombreAnimal + "]";
	}

}
