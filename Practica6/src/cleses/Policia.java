package cleses;

public class Policia {

	private String nombre;
	private String dni;
	private String localidad;
	private int a�os;
	private int antiguedad;
	private double salario;
	
	public Policia(String nombre, String dni, String localidad, int a�os, int antiguedad, double salario) {
		
		this.nombre = nombre;
		this.dni = dni;
		this.localidad = localidad;
		this.a�os = a�os;
		this.antiguedad = antiguedad;
		this.salario = salario;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public int getA�os() {
		return a�os;
	}

	public void setA�os(int a�os) {
		this.a�os = a�os;
	}

	public int getAntiguedad() {
		return antiguedad;
	}

	public void setAntiguedad(int antiguedad) {
		this.antiguedad = antiguedad;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}
	@Override
	public String toString() {
		return "Policia [nombre=" + nombre + ", dni=" + dni + ", localidad=" + localidad + ", a�os=" + a�os
				+ ", antiguedad=" + antiguedad + ", salario=" + salario + "]";
	}

}
