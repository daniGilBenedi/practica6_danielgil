package cleses;

public class PoliciaNacional extends Policia {

	private String arma;
	private final double plus = 100.67;

	public PoliciaNacional(String nombre, String dni, String localidad, int a�os,
			int antiguedad, double salario, String arma) {
		super(nombre, dni, localidad, a�os, antiguedad, salario);
		this.arma = arma;
	}
	
	
	public PoliciaNacional(String nombre, String dni, String localidad, int a�os,
			int antiguedad, double salario) {
		super(nombre, dni, localidad, a�os, antiguedad, salario);
		
	}


	public double getPlus() {
		return plus;
	}


	public String getArma() {
		return arma;
	}

	public void setArma(String arma) {
		this.arma = arma;
	}
	@Override
	public String toString() {
		return "PoliciaNacional " + super.toString() + "[arma=" + arma + ", plus=" + plus + "]";
	}

}
