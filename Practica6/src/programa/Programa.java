package programa;

import java.util.Scanner;

import cleses.Comisaria;

public class Programa {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Comisaria comisariaMontessori = new Comisaria();
		int numeroSeleccion = 0;
		Scanner input = new Scanner(System.in);
		do {
			
			System.out.println("Eliga una de las siguientes opciones");
			System.out.println("1. Crear una comisaria");
			System.out.println("2. Dar de alta y listar 3 policias nacionales");
			System.out.println("3. Buscar un policia nacional por dni");
			System.out.println("4. Eliminar un policia nacional por dni y listar");
			System.out.println("5. Dar de alta y listar 3 policas de caballeria");
			System.out.println("6. Buscar un policia de caballeria por dni");
			System.out.println("7. Eliminar un policia de caballeria por dni y listar");
			System.out.println("8. Listar un policia de caballeria por a�o");
			System.out.println("9. A�adir polcias a un comisario y listar el comisario");
			System.out.println("10. Cambiar policia de regimineto y mostrar todos los policias de la comisaria");
			System.out.println("11. Jubilar un caballo del regimiento de caballeria por nombre del caballo");
			System.out.println("12. Mostrar toda la comisaria");
			System.out.println("13. Salir de la aplicaci�n");
			
			numeroSeleccion = input.nextInt();
			input.nextLine();
			switch (numeroSeleccion) {
			
			case 1:
					comisariaMontessori.altaComisario("Paco", "10", "Madrid", 50, 25, 1234);
					System.out.println("Su comisaria del Montessori ha sido creada con exito!!");
				break;
			case 2:
				comisariaMontessori.altaNacional("Maria", "3", "jarque", 34, 12, 1234.89, "pistola");
				comisariaMontessori.altaNacional("Lucas", "4", "gotor", 27, 5, 2134.89, "porra");
				/*for (int i = 0; i < 3; i++) {
					System.out.println("Introduce los datos de policia numero "+ (i+1));
					System.out.println("Introduce el nombre del policia(String)");
					String nombre = input.nextLine();
					System.out.println("Introduce el DNI del policia(String)");
					String dni = input.nextLine();
					System.out.println("Introduce la localidad del policia(String)");
					String localidad= input.nextLine();
					System.out.println("Introduce los a�os del policia(int)");
					int a�os = input.nextInt();
					System.out.println("Introduce la antiguedad del policia(int)");
					int antiguedad = input.nextInt();
					System.out.println("Introduce el salario del policaia(double)");
					double salario = input.nextDouble();
					input.nextLine();
					System.out.println("Introduce el arma del policia(string)");
					String arma = input.nextLine();
					comisariaMontessori.altaNacional(nombre, dni, localidad, a�os, antiguedad, salario, arma);
				}*/
				System.out.println("Has creado 3 policias nacionales");
				comisariaMontessori.listarPoliciaNacional();
				break;
			case 3:
				System.out.println("Introduce el DNI del policia Nacional que quieras buscar ");
				String dni3 = input.nextLine();
				comisariaMontessori.buscarPoliciaNacional(dni3);
				break;
			case 4:
				System.out.println("Introduce el DNI del policia Nacional que quieras eliminar");
				String dni4 = input.nextLine();
				comisariaMontessori.borrarPoliciaNacional(dni4);
				comisariaMontessori.listarPoliciaNacional();
				break;
			case 5:
				comisariaMontessori.altaCaballeria("carlos", "1","brea", 23, 3, 1234.56, 4, "Paco");
				comisariaMontessori.altaCaballeria("daniel", "2","illueca", 45, 2, 1564.56, 8, "Rayo");
				/*for (int i = 0; i < 3; i++) {
					System.out.println("Introduce los datos de policia numero "+ (i+1));
					System.out.println("Introduce el nombre del policia(String)");
					String nombre = input.nextLine();Comisaria comisariaMontessori = new Comisaria();
					System.out.println("Introduce el DNI del policia(String)");
					String dni = input.nextLine();
					System.out.println("Introduce la localidad del policia(String)");
					String localidad= input.nextLine();
					System.out.println("Introduce los a�os del policia(int)");
					int a�os = input.nextInt();
					System.out.println("Introduce la antiguedad del policia(int)");
					int antiguedad = input.nextInt();
					System.out.println("Introduce el salario del policaia(double)");
					double salario = input.nextDouble();
					input.nextLine();
					System.out.println("Introduce el nombre del caballo(String)");
					String nombreAnimal = input.nextLine();
					System.out.println("Introduce los a�os del caballo(int)");
					int a�osAnimal = input.nextInt();
					input.nextLine();
					comisariaMontessori.altaCaballeria(nombre, dni, localidad, a�os, antiguedad, salario, a�osAnimal, nombreAnimal);
				}*/
				System.out.println("Has creado 3 policias nacionales");
				comisariaMontessori.listarPoliciaCaballeria();
				break;
			case 6:
				System.out.println("Introduce el DNI del policia de caballeria que quieras buscar ");
				String dni6 = input.nextLine();
				comisariaMontessori.buscarPoliciaCaballeria(dni6);
				break;
			case 7:
				System.out.println("Introduce el DNI del policia de caballeria que quieras eliminar");
				String dni7 = input.nextLine();
				comisariaMontessori.borrarPoliciaCaballeria(dni7);
				comisariaMontessori.listarPoliciaCaballeria();
				break;
			case 8:
				System.out.println("Introduce el a�o para filtrar los policias");
				int a�o8 = input.nextInt();
				comisariaMontessori.listarPoliciaNacionalA�o(a�o8);
				break;
			case 9:
				comisariaMontessori.a�adirPersonalAlComisario();
				break;
			case 10:
				
				comisariaMontessori.cambiarDeRegimiento();
				comisariaMontessori.listarComisario();
				break;
				
			case 11:
				System.out.println("Introduce el nombre del caballo que vas a jubilar");
				String nombreCaballoJubilar = input.nextLine();
				comisariaMontessori.jubilarCaballo(nombreCaballoJubilar);
				break;
			case 12:
				comisariaMontessori.mostrarComisaria();
				break;
			case 13:
				System.out.println("La aplicaci�n ha finalizado");
				break;
			default:
				System.out.println("Vuelva a elegir un numero correcto");
				break;
			}
			
		} while (numeroSeleccion != 13);
		input.close();
	}

}
